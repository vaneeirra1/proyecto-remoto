
package org.example;

import java.util.Scanner;

public class CuentaBanck {

    static String nombreUsuario;
    static double saldo;

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        operativaBancaria();
    }


    public static void depositar(double monto) {
        saldo += monto;
        consultarSaldo();
    }


    public static void retirar(double monto){
        if(saldo >= monto){
            saldo -= monto;
            consultarSaldo();
        } else{
            System.out.println("Saldo insuficiente");
        }
    }

    public static void consultarSaldo(){
        System.out.println("Su saldo actual es: " + saldo);
    }

    public static void aplicarAccion(String accion){
        System.out.println("Ingrese monto a " + accion);
        double monto = scanner.nextDouble();
        if(accion.equals("depositar")){
            depositar(monto);
        } else if (accion.equals("retirar")) {
            retirar(monto);
        }
    }




    private static void operativaBancaria() {
        int opcionUsuario = -1;
        System.out.println("Ingrese su nombre " );
        nombreUsuario = scanner.next();
        System.out.println("Bienvenido " + nombreUsuario);
        while (opcionUsuario != 0) {
            System.out.println("Ingrese la opción deseada");
            System.out.println("1- Consultar Saldo");
            System.out.println("2- Depositar");
            System.out.println("3- Retirar");
            System.out.println("0- Salir");
            opcionUsuario = scanner.nextInt();

            switch (opcionUsuario) {
                case 1:
                    consultarSaldo();
                    break;
                case 2:
                    aplicarAccion("depositar");
                    break;
                case 3:
                    aplicarAccion("retirar");
                    break;
                case 0:
                    System.out.println("Gracias!! Totales....");
            }
        }
    }
}
