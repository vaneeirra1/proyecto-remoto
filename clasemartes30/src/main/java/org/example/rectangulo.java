package org.example;

import java.util.Scanner;

public class rectangulo {

    //atributos
    static double longuitud;
    static double ancho;
   static double area;
   static double perimetro;

   static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int opcionUsuario = -1;

        System.out.println("Ingrese longuitud");
        longuitud = scanner.nextInt();
        System.out.println("Ingrese ancho");
        ancho = scanner.nextInt();

        while (opcionUsuario != 0) {
            System.out.println("Ingrese la opción deseada");
            System.out.println("1- Calcular Area");
            System.out.println("2- Calcular Perimetro");
            System.out.println("0- Salir");
            opcionUsuario = scanner.nextInt();

             switch (opcionUsuario) {
            case 1:
                calcularArea();
                break;
            case 2:
                calcularPerimetro();
                break;
                 case 0:
                     System.out.println("Gracias");
        }
    }
}

    private static void calcularPerimetro() {
        perimetro = (longuitud + longuitud) * (ancho + ancho);
        System.out.println("El perimetro es " + perimetro);
    }

    private static void calcularArea() {
        area = longuitud * ancho;
        System.out.println("El area es " + area);
    }
    }
