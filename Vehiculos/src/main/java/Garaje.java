import java.util.ArrayList;

class Garaje {
    private ArrayList<Vehiculo> vehiculos;

    public Garaje() {
        vehiculos = new ArrayList<>();
    }

    public void agregarVehiculo(Vehiculo vehiculo) {
        vehiculos.add(vehiculo);
    }

    public void mostrarCostosMantenimiento() {
        for (Vehiculo v : vehiculos) {
            System.out.println("Vehículo: " + v.getMarca() + " " + v.getModelo() + " - Costo Mantenimiento: " + v.calcularCostoMantenimiento());
        }
    }

    public void mostrarCoches() {
        for (Vehiculo v : vehiculos) {
            if (v instanceof Coche) {
                System.out.println("Coche: " + v.getMarca() + " " + v.getModelo());
            }
        }
    }
}
