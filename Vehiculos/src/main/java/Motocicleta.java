class Motocicleta extends Vehiculo {
    private int cilindrada;

    public Motocicleta(String marca, String modelo, int anio, double kilometraje, int cilindrada) {
        super(marca, modelo, anio, kilometraje);
        this.cilindrada = cilindrada;
    }

    @Override
    public double calcularCostoMantenimiento() {
        return kilometraje * 0.03 + cilindrada * 0.02;
    }
}
