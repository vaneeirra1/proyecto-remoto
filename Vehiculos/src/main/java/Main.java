public class Main {
    public static void main(String[] args) {
        Garaje garaje = new Garaje();

        Coche coche1 = new Coche("Toyota", "Corolla", 2018, 50000, 4);
        Motocicleta moto1 = new Motocicleta("Yamaha", "YZF-R3", 2020, 12000, 321);

        garaje.agregarVehiculo(coche1);
        garaje.agregarVehiculo(moto1);

        garaje.mostrarCostosMantenimiento();
        garaje.mostrarCoches();
    }
}

