class Coche extends Vehiculo {
    private int numPuertas;

    public Coche(String marca, String modelo, int anio, double kilometraje, int numPuertas) {
        super(marca, modelo, anio, kilometraje);
        this.numPuertas = numPuertas;
    }

    @Override
    public double calcularCostoMantenimiento() {
        return kilometraje * 0.05 + numPuertas * 10;
    }
}
