import java.util.Scanner;

public abstract class Empleado {
    //atributos
    String nombreEmpleado;
    int edad;
    double salarioBase = 25000;
    double salarioTotal;

    public abstract void calcularSalario();
    public void mostrarInformacion(){
        System.out.println("Nombre: " + nombreEmpleado);
        System.out.println("Edad: " + edad);
        System.out.println("Salario Total: " + salarioTotal);
    }
}


