import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Empresa {

    static Scanner scanner = new Scanner(System.in);
    static List<Empleado> listaDeEmpleado = new ArrayList<>();

    public static void main(String[] args) {
        for (int i = 0; i < 2; i++) {
            String nombre = pedirNombre();
            int edad = pedirEdad();
            int tipoEmpleado = pedirTipoEmpleado();
            switch (tipoEmpleado) {
                case 1:
                    EmpleadoTiempoCompleto emp1 = new EmpleadoTiempoCompleto();
                    emp1.nombreEmpleado = nombre;
                    emp1.edad = edad;
                    emp1.calcularSalario();
                    agregarEmpleado(emp1);
                    break;
                case 2:
                    EmpleadoMedioTiempo emp2 = new EmpleadoMedioTiempo();
                    emp2.nombreEmpleado = nombre;
                    emp2.edad = edad;
                    emp2.horasTrabajadas = pedirHorasTrabajadas();
                    emp2.calcularSalario();
                    agregarEmpleado(emp2);
                    break;
                case 3:
                    EmpleadoContratista emp3 = new EmpleadoContratista();
                    emp3.nombreEmpleado = nombre;
                    emp3.edad = edad;
                    emp3.horasTrabajadas = pedirHorasTrabajadas();
                    emp3.calcularSalario();
                    agregarEmpleado(emp3);
                    break;
                default:
                    break;
            }
        }
        calcularSalariosTotales();
        mostrarInformacionEmpleados();
    }


    private static String pedirNombre() {
        System.out.println("Ingrese nombre del empleado");
        return scanner.next();
    }

    private static int pedirEdad() {
        System.out.println("Ingrese edad");
        return scanner.nextInt();
    }

    private static int pedirTipoEmpleado(){
        System.out.println("ingrese tipo de empleado");
        System.out.println("1- Tiempo completo");
        System.out.println("2- Medio tiempo");
        System.out.println("3- Contratista");
        return scanner.nextInt();
    }

    private static double pedirHorasTrabajadas() {
        System.out.println("Ingrese la cantidad de horas trabajadas");
        return scanner.nextDouble();
    }

    public static void agregarEmpleado(Empleado empleado){
        listaDeEmpleado.add(empleado);
    }

    public static void calcularSalariosTotales(){
        double salarioAcumulado = 0;
        for(Empleado emp: listaDeEmpleado) {
            salarioAcumulado += emp.salarioTotal;
        }
    }

    public static void mostrarInformacionEmpleados(){
        for(Empleado emp: listaDeEmpleado) {
            emp.mostrarInformacion();
        }

    }

}
