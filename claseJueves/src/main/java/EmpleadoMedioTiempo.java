public class EmpleadoMedioTiempo extends Empleado {
    double horasTrabajadas;
    double tarifa_hora = 120.5;

    public void calcularSalario() {
        salarioTotal = tarifa_hora * horasTrabajadas;
    }

    public void mostrarInformacion(){
        super.mostrarInformacion();
        System.out.println("Horas trabajadas: " + horasTrabajadas);
        System.out.println("-------------------------");
    };
}
