public class EmpleadoContratista extends Empleado{
    double tarifaPorHora = 125.5;
    double horasTrabajadas;

    public void calcularSalario() {
        salarioTotal = tarifaPorHora * horasTrabajadas;
    }

    public void mostrarInformacion(){
        super.mostrarInformacion();
        System.out.println("Horas trabajadas: " + horasTrabajadas);
        System.out.println("-------------------------");
    };
}
