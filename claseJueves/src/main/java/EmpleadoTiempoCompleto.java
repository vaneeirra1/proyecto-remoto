import java.util.Scanner;

public class EmpleadoTiempoCompleto extends Empleado {
    double bono = 5000;

    public void calcularSalario(){
        salarioTotal = salarioBase + bono;
    }

    public void mostrarInformacion(){
        super.mostrarInformacion();
        System.out.println("-------------------------");
    };
}
