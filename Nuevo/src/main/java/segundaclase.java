import java.util.Scanner;

public class segundaclase {

    //Atributos
    static String modelo= "CASSIO";
    static double numero1;
    static double numero2;
    static double resultado;
    static String funcion;
    static int opcionUsuario = -1;

    // Objeto
    static Scanner scanner= new Scanner(System.in);

    // Funcionalidades
    public static void main(String[] args) {
        ejecutarProceso();
    }

    // Nuevas funciones
    private static double solicitarNumero() {
        System.out.println("Ingrese número");
        return scanner.nextInt();
    }

    private static int seleccionDeOpciones() {
        System.out.println("Ingrese operación a realizar:");
        System.out.println("1 - Sumar");
        System.out.println("2 - Restar");
        System.out.println("3 - Dividir");
        System.out.println("4 - Multiplicar");
        System.out.println("0 - Finalizar");
        return scanner.nextInt();
    }

    public static void ejecutarProceso() {
        System.out.println("Bienvenido a la calculadora " + modelo);
        numero1 = solicitarNumero();
        while(opcionUsuario != 0) {
            opcionUsuario = seleccionDeOpciones();
            if (opcionUsuario != 0) {
                numero2 = solicitarNumero();
            }
            procesarOperacion(opcionUsuario);
        }
        System.out.println("El resultado final es: " + resultado);
    }

    private static void procesarOperacion(int operacion) {
        if (resultado == 0) {
            resultado = numero1;
        }
        switch(operacion) {
            case 1:
                sumar(resultado, numero2);
                System.out.println("El resultado parcial es: " + resultado);
                break;
            case 2:
                restar(resultado, numero2);
                System.out.println("El resultado parcial es: " + resultado);
                break;
            case 3:
                dividir(resultado, numero2);
                System.out.println("El resultado parcial es: " + resultado);
                break;
            case 4:
                multiplicar(resultado, numero2);
                System.out.println("El resultado parcial es: " + resultado);
                break;
            case 0:
                break;
            default:
                System.out.println("Debe ingresar una opción válida");
                break;
        }
    }

    // Funciones existentes en primera etapa
    private static void seleccionarFuncion() {

        funcion = scanner.next();
        if(funcion.equals("sumar")){
            ingresarValores();
            sumar(numero1, numero2);
            System.out.println("El resultado de la suma es" +resultado);
        } else if(funcion.equals("restar")){
            ingresarValores();
            restar(numero1, numero2);
        } else if(funcion.equals("dividir")){
            ingresarValores();
            dividir(numero1, numero2);
        } else if (funcion.equals("multiplicar")) {
            ingresarValores();
            multiplicar(numero1, numero2);
        } else if (funcion.equals("combinaciones")) {
            ingresarValores();
            combinacionDeFunciones();
        } else{
            System.out.println("si lo seleccionado anteriormente no esta dentro de los parametros, entonces es nulo");
        }
    }

    public static void sumar(double num1, double num2) {
        resultado = num1 + num2;
    }

    private static void restar(double num1, double num2) {
        resultado = num1 - num2;
    }

    private static void multiplicar(double num1, double num2) {
        resultado = num1 * num2;
    }

    private static void dividir(double num1, double num2) {
        resultado = num1 / num2;
    }

    private static void ingresarValores() {
        System.out.println("Ingrese numero1");
        numero1 = scanner.nextInt();
        System.out.println("Ingrese numero2");
        numero2 = scanner.nextInt();
    }

    public static void combinacionDeFunciones() {
        resultado = numero1 + numero2 * numero2 / numero1;
        System.out.println("El resultado es" + resultado);
        resultado = numero1 + numero2;
        resultado = resultado + numero2;
        resultado = resultado * numero1;
        resultado = resultado / numero2;
        resultado = resultado - numero1;
        System.out.println("El resultado obtenido es" + resultado);
    }

}



