import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListaEnteros {

    static List<Integer> numerosEnteros = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int opcion =-1;
        int num = -1;
        while(opcion !=0 ){
            opcion = getOpcion();
            switch (opcion){
                case 1:
                    num = pedirNumero();
                    agregarEntero(num);
                    break;
                case 2:
                    num = pedirNumero();
                    eliminarEntero(num);
                    break;
                case 3:
                    mostrarLista();
                    break;
                case 0:
                    System.out.println("adios");
                    break;
            }
        }

    }

    private static int getOpcion() {
        int opcion;
        System.out.println("menu de opciones");
        System.out.println("1- agregar");
        System.out.println("2- eliminar");
        System.out.println("3- mostrar lista");
        System.out.println("0- salir");
        opcion = scanner.nextInt();
        return opcion;
    }

    private static Integer pedirNumero() {
        System.out.println("Ingrese numero");
        return scanner.nextInt();
    }

    public static void agregarEntero(Integer num){
        if(numerosEnteros.contains(num)){
            System.out.println("el numero ya se encuentra en la lista");
        }
        else {
            numerosEnteros.add(num);
            System.out.println("se agrego correctamente");
        }
    }

    public static void eliminarEntero(Integer num){
       if(numerosEnteros.contains(num)) {
           if (numerosEnteros.remove(num)) {
               System.out.println("se elimino correctamente");
           }
       }
       else {
           System.out.println("el numero no se encuentra en la lista");
       }
    }

    public static void mostrarLista(){
        System.out.println(numerosEnteros);
    }
}
