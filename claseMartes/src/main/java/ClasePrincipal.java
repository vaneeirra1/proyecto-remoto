import java.util.Scanner;

public class ClasePrincipal extends Cronometro {

    public static void main(String[] args) {
         int opcionUsuario =-1;
        Scanner scanner = new Scanner(System.in);
        iniciar();
        long tiempoInicio = System.currentTimeMillis();
        while (true) {

            long tiempoActual = System.currentTimeMillis() - tiempoInicio;
            System.out.println(formatoTiempo(tiempoActual));

            try {
                pausar();
                continuar();
                reiniciar();

                // Actualiza cada 1000 milisegundos
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
