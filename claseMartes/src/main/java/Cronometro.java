import java.util.Date;

public class Cronometro {
    //atributos
    static long inicio = 0;
    static long pausa = 0;
    static long continuo = 0;
    static boolean pausado = false;
    static long tiempoTotal = 0;


    public static void iniciar(){
        inicio = System.currentTimeMillis();
        Date fecha = new Date();
        tiempoTotal = inicio;
        //System.out.println(tiempoTotal);
        //System.out.println("Fecha: " + fecha);
    }

    public static void reiniciar(){
        inicio = 0;
        pausa = 0;
        continuo = 0;
        pausado = false;

    }

    public static void continuar(){
        if(pausado){
            pausado = false;
            continuo = pausa;
            continuo = System.currentTimeMillis();
        }
    }

    public static void pausar(){
        if (!pausado){
            pausado = true;
            pausa = System.currentTimeMillis();
        } else {

        }
    }

    public static String formatoTiempo(long tiempoTotal) {
        long milisegundos = tiempoTotal % 1000;
        long segundos = (tiempoTotal / 1000) % 60;
        long minutos = (tiempoTotal / (1000 * 60)) % 60;
        long horas = (tiempoTotal / (1000 * 60 * 60));
        return String.format("%02d:%02d:%02d:%03d", horas, minutos, segundos, milisegundos);
    }
}