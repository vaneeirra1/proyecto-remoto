import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class zoologico {

    static List<Animal> listadeAnimales = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);
    static int opcionUsuario = -1;

    public static void main(String[] args) {

        System.out.println("----BIENVENIDO AL ZOOLOGICO----");

        while (opcionUsuario != 0) {
            mostrarMenu();
            switch (opcionUsuario) {
                case 1:
                    agregarAnimal();
                    break;
                case 2:
                    mostrarListaAnimales();
                    break;
                case 3:
                    eliminarAnimalDeLaLista();
                    break;
                case 4:
                    mostrarLaAlimentacionDiaria();
                    break;
                case 0:
                    System.out.println("GRACIAS");
                    break;
            }
        }
    }

    public static void mostrarMenu() {
        System.out.println("1- Agregar Animal");
        System.out.println("2- Mostar Lista Animales");
        System.out.println("3- Borrar Animal");
        System.out.println("4- Mostrar Alimentacion");
        System.out.println("0- Salir");
        opcionUsuario = scanner.nextInt();
    }


    public static void agregarAnimal() {
        System.out.println("Ingrese nombre");
        String nombreAnimal = scanner.next();
        System.out.println("Ingrese edad");
        int edadAnimal = scanner.nextInt();
        System.out.println("Ingrese peso");
        double pesoAnimal = scanner.nextDouble();
        System.out.println("elija tipo de especie");
        System.out.println("1- mamifero");
        System.out.println("2- ave");
        int tipoAnimal = scanner.nextInt() == 1 ? 1 : 2;
        if (tipoAnimal == 1) {
            Mamifero mamifero = new Mamifero();
            mamifero.setNombre(nombreAnimal);
            mamifero.setEdad(edadAnimal);
            mamifero.setPeso(pesoAnimal);
            listadeAnimales.add(mamifero);
        } else {
            Ave ave = new Ave();
            ave.setNombre(nombreAnimal);
            ave.setEdad(edadAnimal);
            ave.setPeso(pesoAnimal);
            listadeAnimales.add(ave);
        }
    }

    public static void mostrarListaAnimales() {
        System.out.println(listadeAnimales);
    }

    public static void mostrarLaAlimentacionDiaria() {
        for (Animal a : listadeAnimales) {
            System.out.println("Alimentacion: " + a.alimentacion);

        }
    }

    public static void eliminarAnimalDeLaLista() {
        System.out.println("ingrese el nombre del animal a borrar");
        String nombreAnimal = scanner.next();
        for (Animal a : listadeAnimales) {
            if (a.getNombre().equals(nombreAnimal)) {
                listadeAnimales.remove(a);
            }
        }
    }
}
