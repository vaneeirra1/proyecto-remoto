public class Mamifero extends Animal {
    boolean esCarnivoro = true;


    @Override
    public void calcularAlimentacionDiaria() {
        if(esCarnivoro){
            alimentacion = peso * 0.05;
        }
        else {
            alimentacion = peso + 0.03;
        }
    }
}
